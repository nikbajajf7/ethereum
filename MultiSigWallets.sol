// SPDX-License-Identifier: GPL-3.0

pragma solidity >= 0.8.1 < 0.9.0;

contract MultiSigWallet {

    uint public confirmRequestRequired;
    mapping(address => bool) exsitingOwners; 
    mapping(uint => mapping(address => bool)) public transactionConfirmations;

    struct Transaction {
        address payable to;
        uint amount; 
        uint priority;
        uint hasConfirmRequests;
        bool isExcuted;
        uint id;
    }

    Transaction[] public transactions;

    constructor(address[] memory _owners, uint _requiredConfirmationNumber) payable{
        require(_owners.length > 0, "has no owners");
        require(_requiredConfirmationNumber > 0, "Required no of confirmations should be greater then 1");
        require(_requiredConfirmationNumber > 0, "Required no of confirmations can not be greater than no of owners");

        for(uint i=0; i < _owners.length; i++) {
            address owner = _owners[i];
            require(owner != address(0), "Invalid owner");
            require(!exsitingOwners[owner], "Owner is not unique");

            exsitingOwners[owner] = true;
        }

            confirmRequestRequired = _requiredConfirmationNumber;
    }

    modifier onlyOwnerCanExecute() {
        require(exsitingOwners[msg.sender], "Owner does not exist");
        _;
    }

    modifier validAddress(address _addr) {
        require(_addr != address(0), "Not valid address");
        _;
    }

    function submitTransaction(address payable _to, uint _amount)
    public
    onlyOwnerCanExecute
    validAddress(_to) {

        transactions.push(Transaction({
            to: _to,
            amount: _amount,
            priority: 0,
            isExcuted: false,
            hasConfirmRequests: 0,
            id: transactions.length
        }));
    }

    function confirmTransaction(uint txIndex)
    public
    onlyOwnerCanExecute
    validAddress(msg.sender) {
        require(txIndex < transactions.length, "Transaction does not exist");

        Transaction storage transaction = transactions[txIndex];

        require(!transaction.isExcuted , "Already excuted");

        require(!transactionConfirmations[txIndex][msg.sender], "Owner already confirmed it");
        
        transactionConfirmations[txIndex][msg.sender] = true;

        transaction.hasConfirmRequests += 1;
        transaction.priority += 1;
    }

    function executeTransactions(uint[] memory txs)
    public
    payable
    onlyOwnerCanExecute
    validAddress(msg.sender) {
        for(uint i; i < txs.length; i++) {
            Transaction storage transaction = transactions[i];
            require(!transaction.isExcuted, "Already excuted");
            require(transaction.hasConfirmRequests >= confirmRequestRequired, "Can not execute");

            (bool success, ) = transaction.to.call{value: transaction.amount}("");
            require(success, "Transaction can not be excuted");

            if(success) {
                transaction.isExcuted = true;
            }
        }
    }

}
